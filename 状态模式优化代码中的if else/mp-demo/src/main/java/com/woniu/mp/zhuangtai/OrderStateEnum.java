package com.woniu.mp.zhuangtai;

/**
 * @className: OrderStateEnum
 * @author: woniu
 * @date: 2023/12/2
 **/
public enum OrderStateEnum {

    //未支付
    UNPAY {
        @Override
        public void nextState(Order order) {
            order.setState(PAID);
        }
    }, //已支付
    PAID {
        @Override
        public void nextState(Order order) {
            order.setState(FINISHED);
        }
    },

    FINISHED {
        @Override
        public void nextState(Order order) {
            order.setState(PAID);
        }
    };


    public abstract void nextState(Order order);


}
