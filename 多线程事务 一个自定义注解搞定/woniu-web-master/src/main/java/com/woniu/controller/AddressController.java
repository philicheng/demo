package com.woniu.controller;

import com.woniu.service.MainService;
import com.woniu.vo.ApiResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * SpringBoot中的多线程事务处理太繁琐？一个自定义注解直接搞定！
 */
@RequestMapping("/api")
@RestController
@Slf4j
public class AddressController {


    @Autowired
    MainService mainService;


    @PostMapping("/get2")
    public ApiResponse get() {
        mainService.test1();
        return ApiResponse.ok(null);
    }




}
