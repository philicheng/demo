package com.woniu.controller;

import com.woniu.service.RedisIdempotentStorage;
import com.woniu.utils.IdGeneratorUtil;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 一个注解，优雅的实现接口幂等性！
 *
 * 关键因素1
 * 幂等唯一标识，就是客户端与服务端一次请求时的唯一标识，一般情况下由客户端来生成，也可以让第三方来统一分配。
 *
 * 关键因素2
 * 有了唯一标识以后，服务端只需要确保这个唯一标识只被使用一次即可，一种常见的方式就是利用数据库的唯一索引。
 */
@RestController
@RequestMapping("/generate")
public class IdGeneratorController {
    @Resource
    private RedisIdempotentStorage redisIdempotentStorage;

    @RequestMapping("/getIdGeneratorToken")
    @ResponseBody
    public String getIdGeneratorToken() {
        String generateId = IdGeneratorUtil.generateId();
        redisIdempotentStorage.save(generateId);
        return generateId;
    }

}
