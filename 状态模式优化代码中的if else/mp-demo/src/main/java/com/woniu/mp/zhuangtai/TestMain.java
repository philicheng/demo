package com.woniu.mp.zhuangtai;


/**
 * 使用状态模式优化代码 解决代码中的if else
 */
public class TestMain {
    // Order  未支付-> 已支付-> 已完成
    public static void main(String[] args) {
        Order order = new Order();
//        if (order.getState() == "") {
//            order.setState(OrderStateEnum.PAID);
//        }
        System.out.println(order.getState());
        order.nextState();
        System.out.println(order.getState());
        order.nextState();
        System.out.println(order.getState());
    }
}
