package com.woniu.utils;

import java.util.UUID;

public class IdGeneratorUtil {

    public static String generateId() {
        return UUID.randomUUID().toString();
    }
}
