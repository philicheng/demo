package com.woniu.thread;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;


/**
 * 加了一行log日志，结果引发了线上事故
 */
@Slf4j
public class FastJonTest {

    public static void main(String[] args) {
        StudentDTO studentDTO = new StudentDTO();
        String str = JSON.toJSONString(studentDTO);
        log.info(str);
    }


}
