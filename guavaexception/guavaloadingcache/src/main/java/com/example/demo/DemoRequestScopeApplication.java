package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Guava自加载缓存LoadingCache
 */
@SpringBootApplication
public class DemoRequestScopeApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoRequestScopeApplication.class, args);
	}

}
