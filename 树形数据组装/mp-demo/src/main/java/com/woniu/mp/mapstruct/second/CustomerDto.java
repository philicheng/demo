package com.woniu.mp.mapstruct.second;

import lombok.Data;

import java.util.Date;

@Data
public class CustomerDto {
    public Long id;
    public String customerName;

    private String format;
    private Date time;
}

