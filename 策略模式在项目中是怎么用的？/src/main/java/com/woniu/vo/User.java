package com.woniu.vo;

import lombok.Data;

@Data
public class User {
 private Long id;
 private String name;
 private Integer sex;
}
