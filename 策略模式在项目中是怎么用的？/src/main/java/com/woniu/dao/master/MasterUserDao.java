package com.woniu.dao.master;

import com.woniu.vo.User;
import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface MasterUserDao {

    int insertUser(User user);


}
