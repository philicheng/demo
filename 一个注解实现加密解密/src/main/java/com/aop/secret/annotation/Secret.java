package com.aop.secret.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <p>
 * 加解密注解
 * </p>
 *
 * @author 蜗牛
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Secret {

	// 请求解密的属性，默认手机号和身份证号，可以自行扩展。
	String[] reqPropsName() default {"phone","idCard"};

	// 响应加密的属性，默认手机号和身份证号，可以自行扩展。
	String[] respPropsName() default {"phone","idCard"};

}
