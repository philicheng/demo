package com.woniu.service.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.woniu.service.entity.Order;

/**
 * <p>
 *  状态枚举类
 * </p>
 *
 * @author 公众号：【程序员蜗牛g】
 */
public interface IOrderService extends IService<Order> {

}
