package com.example.demo.controller;

import com.example.demo.limit.Limit;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.concurrent.TimeUnit;

/**
 * SpringBoot 如何进行限流？ 单机可以这么玩
 * Guava限流工具类RateLimiter
 */
@Slf4j
@RestController
@RequestMapping("/limit")
public class LimitController {


    @GetMapping("/test")
    @Limit(key = "limit3", permitsPerSecond = 1, timeout = 50, timeunit = TimeUnit.MILLISECONDS,msg = "系统繁忙，请稍后再试！")
    public String limit3() {
        log.info("令牌桶limit3获取令牌成功");
        return "ok";
    }

}